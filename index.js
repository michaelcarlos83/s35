const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv') //use to hide pass within the code

dotenv.config()

const app = express()
const port = 3001


//MongoDB Connection
mongoose.connect(`mongodb+srv://mike18:${process.env.MONGODB_PASSWORD}@cluster0.hps6m9a.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))
//MongoDB Connection End

app.use(express.json())
app.use(express.urlencoded({extended: true}))


//MongoDB Schemas
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type:String,
		defaults: 'Pending'
	}
})
//MongoDB Schemas END


//MongoDB Model
const Task = mongoose.model('Task', taskSchema) //Singular
//MongoDB Model END

// Routes
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}, (error, result) => {
		if (result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		}

		let newTask = new Task({
			name: request.body.name
		})

		newTask.save((error, saveTask) => {
			if(error){
				return console.error(error)
			}
			else{
				return response.status(200).send('New task created')
			}
		})
	})
})

app.get('/tasks', (request, response) => {
	Task.find({}, (error, result) => {
		if (error){
			return console.log(error)
		}

		return response.status(200).json({
			data:result
		})
	})
})


//Activity Start

//MongoDB Schemas
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type:String,
		defaults: 'Pending'
	}
})
//MongoDB Schemas END

const User = mongoose.model('User', userSchema)



app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username, password:request.body.password}, (error, result) => {
		if (result != null && result.username == request.body.name){
			return response.send('Duplicate task found!')
		}

		let newUser = new User({
			username: request.body.username,
			password: request.body.password
		})

		newUser.save((error, saveTask) => {
			if(error){
				return console.error(error)
			}
			else{
				return response.status(200).send('New User Registered!')
			}
		})
	})
})

// Routes End

app.listen(port, () => console.log(`Server running at localhost ${port}`))


